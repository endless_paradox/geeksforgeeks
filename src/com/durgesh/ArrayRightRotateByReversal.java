package com.durgesh;

import java.util.Arrays;

public class ArrayRightRotateByReversal {

    public static void main(String[] args) {
        int[] arr = {1,2,3,4,5,6,7,8,9,10};
        int d = 3;
        int n = arr.length;
        rightRotate(arr, n, d);
        Arrays.stream(arr).forEach(e -> System.out.println(e));
    }

    private static void rightRotate(int[] arr, int n, int d) {
        d = d % n;
        if(d == 0)
            return;
        arrayReversal(arr, 0, n-1);
        arrayReversal(arr, 0, d-1);
        arrayReversal(arr, d, n-1);

    }

    private static void arrayReversal(int[] arr, int start, int end) {
        int temp;
        while(start < end)  {
            temp = arr[start];
            arr[start] = arr[end];
            arr[end] = temp;
            start++;
            end--;
        }
    }
}
