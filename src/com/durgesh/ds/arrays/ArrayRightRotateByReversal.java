package com.durgesh.ds.arrays;

import java.util.Arrays;

public class ArrayRightRotateByReversal {
    public static void main(String[] args) {
        ArrayRightRotateByReversal arrayRightRotateByReversal = new ArrayRightRotateByReversal();
        int[] arr = {1,2,3,4,5,6,7,8,9};
        int d = 2;
        arrayRightRotateByReversal.arrayRotate(arr, d);
        Arrays.stream(arr).forEach(e -> System.out.print(e + " "));
    }

    private void arrayRotate(int[] arr, int d) {
        d = d % arr.length;
        if(d == 0)
            return;
        arrayReversal(arr, 0, arr.length-1);
        arrayReversal(arr, 0, d-1);
        arrayReversal(arr, d, arr.length-1);
    }

    private void arrayReversal(int[] arr, int start, int end) {
        int temp;
        while(start <= end) {
            temp = arr[start];
            arr[start] = arr[end];
            arr[end] = temp;
            start++;
            end--;
        }
    }
}
