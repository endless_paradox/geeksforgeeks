package com.durgesh.ds.linkedlist;

class SingleLList    {
    Node head;
    static class Node   {
        int data;
        Node next;
        Node(int data)  {
            this.data = data;
        }
    }

    public void push(int d) {
        Node node = new Node(d);
        node.next = head;
        head = node;
    }

    public void insertAfter(Node prev_node, int data)   {
        if(prev_node == null)
            return;
        Node node = new Node(data);
        node.next = prev_node.next;
        prev_node.next = node;
    }

    public void append(int data)    {
        Node temp = head;
        while(temp.next != null)
            temp = temp.next;
        Node node = new Node(data);
        temp.next = node;
    }

    public void printList()    {
        Node temp = head;
        while(temp != null) {
            System.out.println(temp.data);
            temp = temp.next;
        }
    }

}

public class LinkedListInsertion {


    public static void main(String[] args) {

        SingleLList singleLList = new SingleLList();
        singleLList.head = new SingleLList.Node(10);
        singleLList.append(11);
        singleLList.append(14);
        singleLList.insertAfter(singleLList.head.next, 12);
        singleLList.insertAfter(singleLList.head.next.next, 13);
        singleLList.push(9);
        singleLList.push(8);
        singleLList.printList();
    }
}
