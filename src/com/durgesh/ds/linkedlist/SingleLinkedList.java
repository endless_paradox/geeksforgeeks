package com.durgesh.ds.linkedlist;

public class SingleLinkedList {

    public static void main(String[] args) {
        LinkedList llist = new LinkedList();
        llist.head = new LinkedList.Node(1);
        llist.push(2);
        llist.push(3);
        llist.insertAfter(0, llist.head.next);
        llist.append(-1);
        llist.append(-2);
        llist.deleteNode(-1);
        llist.printList();
    }
}

class LinkedList    {
    Node head;
    static class Node  {
        int d;
        Node next;

        Node(int d) {
            this.d = d;
        }
    }
    public void printList() {
        Node n = head;
        while(n != null)    {
            System.out.println(n.d);
            n = n.next;
        }
    }

    public void push(int new_data)  {
        Node n = new Node(new_data);
        n.next = head;
        head = n;
    }

    public void insertAfter(int new_data, Node prev_node)   {
        if(prev_node == null)
            return;
        Node n = new Node(new_data);
        n.next = prev_node.next;
        prev_node.next = n;
    }

    public void append(int new_data)    {
        Node endNode = head;
        while(endNode.next != null)
            endNode = endNode.next;
        Node n = new Node(new_data);
        endNode.next = n;
    }

    public void deleteNode(int data)    {
        Node temp = head, prev = null;
        if(head.d == data)  {
            head = temp.next;
            return;
        }
        while(temp != null && temp.d != data)   {
            prev = temp;
            temp = temp.next;
        }
        if(temp == null)
            return;
        prev.next = temp.next;
    }
}
