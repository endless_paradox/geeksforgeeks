package com.durgesh;

import java.util.Arrays;

public class CountingSort {

    public static void main(String[] args) {
        int[] arr = {5,4,2,5,6,-2,1,-9};
        arr = sort(arr);
        Arrays.stream(arr).forEach(System.out::println);
    }

    private static int[] sort(int[] arr) {
        int max = Arrays.stream(arr).max().orElse(0);
        int min = Arrays.stream(arr).min().orElse(0);
        int[] countArray = new int[max - min +1];
        Arrays.stream(arr).forEach(e -> countArray[e - min]++);
        for(int i = 1; i< countArray.length; i++)   {
            countArray[i] += countArray[i-1];
        }
        int[] sortedArray = new int[arr.length];
        for(int i = arr.length - 1; i>=0; i--)  {
            countArray[arr[i] - min]--;
            sortedArray[countArray[arr[i] - min]] = arr[i];
        }
        return sortedArray;
    }
}
