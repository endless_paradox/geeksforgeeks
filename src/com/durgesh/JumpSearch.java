package com.durgesh;

import java.util.stream.IntStream;

public class JumpSearch {

    public static int search(int[] arr, int element)   {
        int n = arr.length;
        int step = (int)Math.floor(Math.sqrt(n));
        int prev = 0;
        while(arr[Math.min(step,n) -1] < element) {
            prev = step;
            step += (int)Math.floor(Math.sqrt(n));
            if(prev >= n)
                return -1;
        }
        return IntStream.range(prev, Math.min(step, n)).filter(i -> arr[i] == element).findFirst().orElse(-1);
    }

    public static void main(String[] args) {
        int[] arr = {2,3,5,56,75,90,556};
        int result = search(arr, 557);
        System.out.println(result == -1?-1:result+1);
    }
}
