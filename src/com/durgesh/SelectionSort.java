package com.durgesh;

import java.util.Arrays;

public class SelectionSort {

    public static void sort(int[] arr)  {
        int len = arr.length;
        for(int i = 0; i < len; i++)    {
            int min_idx = i;
            for(int j = i + 1; j< len; j++) {
                if(arr[j] < arr[min_idx])
                    min_idx = j;
            }
            int temp = arr[i];
            arr[i] = arr[min_idx];
            arr[min_idx] = temp;
        }
    }

    public static void main(String[] args) {
        int[] arr = {8,5,3,56,2,4,556,75,90};
        sort(arr);
        Arrays.stream(arr).forEach(e -> System.out.print(e + " "));
    }
}
