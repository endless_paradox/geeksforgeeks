package com.durgesh;

import java.util.Arrays;

public class ReversingAnArray {

    public static void arrayReversal(int[] arr, int start, int end) {
        int temp;
        if(start >= end)
            return;
        temp = arr[start];
        arr[start] = arr[end];
        arr[end] = temp;
        arrayReversal(arr, start +1, end -1);
    }

    public static void main(String[] args) {
        int[] arr = {1,2,3,4,5,6,7};
        arrayReversal(arr, 0 , arr.length - 1);
        Arrays.stream(arr).forEach(System.out::println);
    }
}
