package com.durgesh;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;

public class Graph {
	
	private final LinkedList<Integer>[] adj;
	
	public Graph(int V)  {
		adj = new LinkedList[V];
		for (int i = 0; i < V; i++) {
			adj[i] = new LinkedList<>();
		}
	}
	
	public void add(int source, int destination)    {
		adj[source].add(destination);
		adj[destination].add(source);
	}
	
	public int bst(int source,int destination) {
		int distance = 0;
		int[] parent = new int[adj.length];
		boolean[] vis = new boolean[adj.length];
		Queue<Integer> q = new LinkedList<>();
		q.add(source);
		parent[source] = -1;
		vis[source] = true;
		while (!q.isEmpty())    {
			int cur = q.poll();
			if(cur == destination)  break;
			for (int neighbour: adj[cur]) {
				if(!vis[neighbour])   {
					vis[neighbour] = true;
					q.add(neighbour);
					parent[neighbour] = cur;
				}
			}
		}
		int cur = destination;
		while (parent[cur] != -1)   {
			System.out.print(cur + " -> ");
			cur = parent[cur];
			distance++;
		}
		System.out.println(cur);
		return distance;
	}
	
	private boolean dfsUtil(int source, int destination, boolean[] vis) {
		if(source == destination) return true;
		for (int neighbour: adj[source]) {
			if (!vis[neighbour])    {
				vis[neighbour] = true;
				boolean isConnected = dfsUtil(neighbour, destination, vis);
				if(isConnected) return true;
			}
			
		}
		return false;
	}
	
	public boolean dfsStack(int source, int destination)    {
		boolean[] vis = new boolean[adj.length];
		Stack<Integer> stack = new Stack<>();
		vis[source] = true;
		stack.push(source);
		while (!stack.isEmpty())    {
			int curr = stack.pop();
			if(curr == destination) return true;
			for (int neighbour: adj[curr])  {
				if(!vis[neighbour]) {
					vis[neighbour] = true;
					stack.push(neighbour);
				}
			}
		}
		return false;
	}
	
	public boolean dfs(int source, int destination)   {
		boolean[] vis = new boolean[adj.length];
		vis[source] = true;
		return dfsUtil(source, destination, vis);
	}
	
	public static void main(String[] args) {
		int edges, vertices;
		System.out.println("Enter the number of vertices: ");
		Scanner sc = new Scanner(System.in);
		vertices = sc.nextInt();
		System.out.println("Enter the number of edges: ");
		edges = sc.nextInt();
		Graph graph = new Graph(vertices);
		for (int i = 0; i < edges; i++) {
			System.out.print("Enter " + i + " edge: ");
			int source, destination;
			source = sc.nextInt();
			destination = sc.nextInt();
			graph.add(source, destination);
		}
		System.out.println(graph.bst(0, 3));
		System.out.println(graph.dfs(0, 1));
		System.out.println(graph.dfsStack(0, 1));
	}
	
}
