package com.durgesh;

import java.util.Arrays;

public class InsertionSort {

    public static void main(String[] args) {
        int[] arr = {8,5,3,56,2,4,556,75,90};
        sort(arr);
        Arrays.stream(arr).forEach(System.out::println);
    }

    private static void sort(int[] arr) {
        for(int i = 1; i< arr.length; i++)  {
            int key = arr[i];
            int j = i-1;
            while(j>=0 && arr[j] > key) {
                arr[j+1] = arr[j];
                j = j-1;
            }
            arr[j+1] = key;
        }
    }
}
