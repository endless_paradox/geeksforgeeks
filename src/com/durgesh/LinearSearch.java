package com.durgesh;

import java.util.stream.IntStream;

public class LinearSearch {

    public static void main(String[] args) {
        int[] arr = {8,5,3,56,2,4,556,75,90};
        int element = 556;
        IntStream.range(0, arr.length).filter(i -> element == arr[i]).findFirst().ifPresent(e -> System.out.println(e+1));
    }
}
