package com.durgesh;

import java.util.Arrays;

public class ArrayRotate {
    public static void main(String[] args) {
        int[] arr = {1,2,3,4,5,6,7};
        int d = 10;
        int n = arr.length;
        rotate(arr, n, d);
        Arrays.stream(arr).forEach(e -> System.out.println(e));
    }

    private static void rotate(int[] arr, int n, int d) {
        d = d % n;
        int[] temp = new int[d];
        for(int i = 0; i < d; i++)  {
            temp[i] = arr[i];
        }
        for(int i = d; i < n; i++ ) {
            arr[i - d] = arr[i];
        }
        for(int i = n-d; i < n; i++)  {
            arr[i] = temp[i - n + d];
        }
    }
}
