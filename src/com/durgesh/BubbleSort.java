package com.durgesh;

import java.util.Arrays;

public class BubbleSort {

    public static void sort(int[] arr)  {
        boolean swapped;
        for(int i = 0; i< arr.length; i++)  {
            swapped = false;
            for(int j = 0; j < arr.length - i -1; j++)  {
                if(arr[j] > arr[j+1])   {
                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                    swapped = true;
                }
            }
            if(!swapped)    {
                break;
            }
        }
    }

    public static void main(String[] args) {
        int[] arr = {8,5,3,56,2,4,556,75,90};
        sort(arr);
        Arrays.stream(arr).forEach(System.out::println);
    }
}
