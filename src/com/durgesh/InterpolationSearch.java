package com.durgesh;

public class InterpolationSearch {

    public static int search(int[] arr, int lo, int hi, int element)    {
        int pos;
        if(lo <= hi && element >= arr[lo] && element <= arr[hi])    {
            pos = lo
                    + (((hi - lo) / (arr[hi] - arr[lo]))
                    * (element - arr[lo]));
            if(arr[pos] == element)
                return pos;
            if(arr[pos] < element)
                return search(arr, pos + 1, hi, element);
            return search(arr, lo, pos - 1, element);
        }
        return -1;
    }

    public static void main(String[] args) {
        int[] arr = {2,3,5,56,75,90,556};
        int index = search(arr, 0, arr.length - 1, 75);
        System.out.println(index);
    }
}
