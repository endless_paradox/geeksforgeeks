package com.durgesh;

public class BinarySearch {

    public static int search(int[] arr, int l, int r, int element) {
        int mid;
        if(r >= l) {
            mid = l + (r - 1) / 2;
            if (arr[mid] == element)
                return mid;
            if (arr[mid] > element)
                return search(arr, l, mid - 1, element);
            return search(arr, mid + 1, r, element);
        }
        return -1;
    }

    public static void main(String[] args) {
        int[] arr = {2,3,5,56,75,90,556};
        int index = search(arr, 0 , arr.length -1, 3);
        System.out.println(index + 1);
    }
}
