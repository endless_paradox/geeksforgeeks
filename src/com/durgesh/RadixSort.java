package com.durgesh;

import java.util.Arrays;
import java.util.stream.IntStream;

public class RadixSort {

    public static void main(String[] args) {
        int[] arr = {12212,2,63434,4,3,7,823,312,934,7434, 5343, 1233};
        radixSort(arr);
        Arrays.stream(arr).forEach(System.out::println);
    }

    private static void radixSort(int[] arr) {
        int max = Arrays.stream(arr).max().orElse(0);
        for(int exp = 1; max/exp > 0; exp *=10)
            countSort(arr, arr.length, exp);
    }

    private static void countSort(int[] arr, int length, int exp) {
        int[] countArray = new int[10];
        int[] sortedArray = new int[length];
        Arrays.fill(countArray, 0);
        for(int i = 0;i< length;i++)    {
            countArray[(arr[i]/exp)%10]++;
        }
        for(int  i =1; i< countArray.length;i++)    {
            countArray[i] += countArray[i-1];
        }
        for(int i = length -1;i >=0;i--)   {
            sortedArray[countArray[(arr[i]/exp)%10] - 1] = arr[i];
            countArray[(arr[i]/exp)%10]--;
        }
        for (int i = 0; i < length; i++)
            arr[i] = sortedArray[i];
    }
}
