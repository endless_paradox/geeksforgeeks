package com.durgesh;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class KnapSackProblem {


    static class ItemValue  {
        Double cost;
        double wt, val, ind;

        ItemValue(double wt, double val, double ind)    {
            this.wt = wt;
            this.val = val;
            this.ind = ind;
            this.cost = val/wt;
        }

        @Override
        public String toString() {
            return "ItemValue{" +
                    "cost=" + cost +
                    ", wt=" + wt +
                    ", val=" + val +
                    ", ind=" + ind +
                    '}';
        }
    }

    public static void main(String[] args) {
        int[] wt = { 10, 40, 20, 30 };
        int[] val = { 60, 40, 100, 120 };
        int capacity = 60;

        double maxValue = getMaxValue(wt, val, capacity);

        // Function call
        System.out.println("Maximum value we can obtain = "
                + maxValue);
    }

    private static double getMaxValue(int[] wt, int[] val, int capacity) {
        ItemValue[] iVal = new ItemValue[wt.length];
        for(int i = 0;i < wt.length;i++)
            iVal[i] = new ItemValue(wt[i], val[i], i);
        Arrays.sort(iVal, (o1,o2) -> o2.cost.compareTo(o1.cost));
        double totalCost = 0d;
        for(ItemValue itemValue: iVal)  {
            int curWt = (int) itemValue.wt;
            int curVal = (int) itemValue.val;
            if(capacity-curWt>=0)   {
                capacity -= curWt;
                totalCost += curVal;
            }
            else {
                double fraction = ((double) capacity/(double) curWt);
                totalCost += (curVal * fraction);
                break;
            }
        }
        return totalCost;
    }


}
