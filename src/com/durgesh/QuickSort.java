package com.durgesh;

import java.util.Arrays;

public class QuickSort {

    public static void main(String[] args) {
        int[] arr = {8,5,3,56,2,4,556,75,90};
        sort(arr, 0 , arr.length-1);
        Arrays.stream(arr).forEach(System.out::println);
    }

    private static void sort(int[] arr, int l, int r) {
        if(l<r) {
            int p = partition(arr, l, r);
            sort(arr, l, p-1);
            sort(arr, p+1, r);
        }
    }

    private static int partition(int[] arr, int l, int r) {

        int pivot = arr[r];
        int i = l -1;
        for(int j = l; j < r; j++)  {
            if(arr[j]<pivot)   {
                i++;
                swap(arr, i, j);
            }
        }
        swap(arr, i+1, r);
        return (i+1);
    }

    private static void swap(int[] arr, int i, int j) {
        int temp = arr[j];
        arr[j] = arr[i];
        arr[i] = temp;
    }
}
