package com.durgesh;

public class ExponentialSearch {

    public static void main(String[] args) {
        int[] arr = {2,3,5,56,75,90,556};
        int element = 75;
        int result = exponentialSearch(arr, element);
        System.out.println(result);
    }

    private static int exponentialSearch(int[] arr, int element) {
        int  i = 1;
        if(arr[0] == element)   {
            return 0;
        }
        while(i < arr.length && arr[i] <= element)
            i = i * 2;

        return search(arr, i/2, Math.min(i, arr.length-1), element);
    }

    public static int search(int[] arr, int l, int r, int element) {
        int mid;
        int low = l;
        int high = r-1;
        while(high >= low) {
            mid = (low + high) >>> 1;
            int midVal = arr[mid];
            if(midVal > element)
                high = mid - 1;
            else if(midVal < element)
                low = mid + 1;
            else    {
                return mid;
            }
        }
        return -(low +1);
    }
}
