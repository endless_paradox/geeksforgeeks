package com.durgesh;

import java.util.Arrays;

public class MergeSort {

    public static void main(String[] args) {
        int[] arr = {8,5,3,56,2,4,556,75,90};
        sort(arr,0,arr.length-1);
        Arrays.stream(arr).forEach(System.out::println);
    }

    private static void sort(int[] arr, int l, int r) {
        if(l<r) {
            int m = l + (r-1)/2;
            sort(arr, l,m);
            sort(arr, m+1, r);
            merge(arr, l, m, r);
        }
    }

    private static void merge(int[] arr, int l, int m, int r) {
        int n1 = m - l + 1;
        int n2 = r - m;

        int[] L = new int[n1];
        int[] R = new int[n2];
        for(int i = 0; i<n1; i++)   {
            L[i] = arr[l+i];
        }
        for(int i = 0; i<n2; i++)   {
            R[i] = arr[m + 1+ i];
        }

        int i =0, j = 0, k = 0;

        while(i<n1 && j <n1)    {
            if(L[i] < R[j]) {
                arr[k] = L[i];
                i++;
            }
            else    {
                arr[k] = R[j];
                j++;
            }
            k++;
        }
        while(i < n1)   {
            arr[k] = L[i];
            i++;
            k++;
        }
        while(j < n2)   {
            arr[k] = R[j];
            j++;
            k++;
        }
    }
}
