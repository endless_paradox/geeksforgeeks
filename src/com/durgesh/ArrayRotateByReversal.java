package com.durgesh;

import java.util.Arrays;

public class ArrayRotateByReversal {

    public static void rotate(int[] arr, int n, int d) {
        d = d%n;
        if(d == 0)
            return;
        arrReversal(arr,0, d-1);
        arrReversal(arr,d, n-1);
        arrReversal(arr,0, n-1);
    }

    private static void arrReversal(int[] arr, int start, int end) {
        int temp;
        while(start < end)  {
            temp = arr[start];
            arr[start] = arr[end];
            arr[end] = temp;
            start++;
            end--;
        }
    }

    public static void main(String[] args) {
        int[] arr = {1,2,3,4,5,6,7};
        int d = 7;
        int n = arr.length;
        rotate(arr, n, d);
        Arrays.stream(arr).forEach(System.out::println);
    }
}
